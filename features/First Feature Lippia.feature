@LTM_25cbc975-81e3-42a9-8873-7bcf72c600a3
Feature: First Feature Lippia

  @Completo @Ready @Stable @LCQ112_000001
  Scenario Outline: 
    Given El usuario inicia sesion con sus credenciales <usuario>, <password>
    When El usuario se dirige a la pantalla 'Administrar Solicitudes'
    And El usuario hace click en 'Pre Ingresar Solicitud' en la pantalla de Administrar Solicitudes
    And El usuario hace click en 'General' en la pantalla Pre Ingresar Solicitud
    And El usuario incompleta los campos de Pre Ingresar Solicitud con los siguientes datos: '<Solicitante>', '<Matriz>', '<Grupo>'
    Then El usuario guarda los cambios

    Examples: 
      | usuario         | password     | Solicitante | Matriz | Grupo |
      | automatizador01 | QA.Prov-2021 | 32003       | 02     | 01    |